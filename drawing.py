# Import Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Creating image
image = np.zeros((512, 512), dtype=np.uint8)
image2 = np.zeros((512, 512, 3), dtype=np.uint8)

# Display image
cv2.imshow("Square", image)

cv2.waitKey(0)
cv2.destroyAllWindows()

# Rectangle
cv2.rectangle(image, (100, 100), (300, 250), (0, 0, 255), 5)

# Line
cv2.line(image, (0, 0), (511, 511), (123, 124, 60), 5)

# Circle: image, centre, radius, color, thickness
cv2.circle(image, (200, 200), 100, (255, 100, 100), -1)

# Polygon
points = np.array([[10, 10], [400, 50], [90, 200], [50, 500]], np.int32)

# Required by OpenCV (Magical Numbers)
points = points.reshape(-1, 1, 2)
cv2.polylines(image, [points], True, (0, 0, 255), 3)

# Text
cv2.putText(image, "YubaAI", (75, 120), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 255), 3)

#

