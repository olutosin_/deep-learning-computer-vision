# Mini program to capture images from pi-camera
# Install dependency
# sudo apt-get update
# suod apt-get upgrade
# q to kill the process


# Import libraries
import cv2
import os
import sys

# Activate the camera
try:
	cap = cv2.VideoCapture(0)

except:
	print("problem opening camera")
	sys.exit(0)

i = 0
image_name = "image"
max_file = 5
while(cap.isOpened()):
    ret, frame = cap.read()
    dim = frame.shape
    dim_1 = dim[0], dim[1]
    img_resized = cv2.resize(frame, dim_1, interpolation = cv2.INTER_AREA)
    cv2.imwrite("%s.%i.jpg" %(image_name,i), img_resized)
    cv2.imshow("Resized Image", img_resized)
    if cv2.waitKey(1) == ord('q'):
        break
    i += 1 

# Closes all opened windows
cv2.destroyAllWindows() 

# Get CWD
my_dir = os.getcwd()

# List CWD
files = []
for file in os.listdir(my_dir):
    if file.endswith(".jpg"): 
         #os.remove(file)
         files.append(list)

print(len(files))



