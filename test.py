# Import libraries

import cv2
import numpy as np
import pandas as pd
import keras
import tensorflow

print(cv2.__version__)
print(keras.__version__)
print(tensorflow.__version__)
