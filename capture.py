# Mini program to capture images from pi-camera
# Install dependency
# sudo apt-get update
# suod apt-get upgrade
# q to kill the process


# Import libraries
import cv2
import time
import os
import sys

# Activate the camera
try:
	capture = cv2.VideoCapture(0)

except:
	print("problem opening camera")
	sys.exit(0)

i = 0
while True:
    img = cv2.imread(capture)
    img_resized = cv2.resize(img, (640, 480), interpolation = cv2.INTER_AREA)
    sleep(2)
    cv.imshow("Resized Image", img_resized)

    cv.imwrite('pic{:>05}.jpg'.format(i), img_resized)
    if cv.WaitKey(1) == ord('q'):
        break
    i += 1 

# Closes all opened windows
cv2.destroyAllWindows() 

