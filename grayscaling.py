# importing libraries
import cv2
print(cv2.__version__)

# loading image
image = cv2.imread('./images/input.jpg')
cv2.imshow('frame', image)
cv2.waitKey(10)
cv2.destroyAllWindows()

# convert GBR image to grayscale
gray_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("frame2", gray_img)
cv2.waitKey(10)
cv2.destroyAllWindows()

# loading image and converts to gray scale
image = cv2.imread('./images/input.jpg', 0)
cv2.imshow('frame', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
