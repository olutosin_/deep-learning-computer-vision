# loading libraries

import cv2
import numpy as np

# making a square
square = np.zeros((300,300), dtype=np.uint8)
cv2.rectangle(square, (50,50), (250,250), 255, -2)
cv2.imshow("Square", square)
cv2.waitKey(0)
cv2.destroyAllWindows()

# making an ellipse
# Syntax: cv2.ellipse(image, centerCoordinates,
# axesLength, angle, startAngle, endAngle, 
#color [, thickness[, lineType[, shift]]])

ellipse = np.zeros((300,300), dtype=np.uint8)
cv2. ellipse(ellipse, (150,150), (150, 150), 30, 0, 180, 255, -1)
cv2.imshow("Ellipse", ellipse)
cv2.waitKey(0)
cv2.destroyAllWindows()

# bitwise operation (works with black and white)
# cv2.bitwise_and, bitwise_or, bitwise_xor, bitwise_not

