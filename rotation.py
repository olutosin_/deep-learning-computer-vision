# Importing Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Read the image
image = cv2.imread("images/input.jpg")
height, width = image.shape[0:2]

Ty = height / 4
Tx = width / 4
"""
    Rotation Matrix
    |cosTheta -sinTheta|
 R= |sinTheta  cosTheta|

"""

# Rotational Matrix: center_x, center_y, angle_of_rotation, scale
M = cv2.getRotationMatrix2D((width/2, height/2), 90, 1)

# Affine Transform
image_transform = cv2.warpAffine(image, M, (width, height))
cv2.imshow("Transformed Image", image_transform)

cv2.waitKey()
cv2.destroyAllWindows()

# Clears the black areas
rotated_image = cv2.transpose(image)
cv2.imshow("Image_Rotated", rotated_image)

cv2.waitKey()
cv2.destroyAllWindows()

