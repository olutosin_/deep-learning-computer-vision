# Importing Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Read the image
image = cv2.imread("images/input.jpg")
height, width = image.shape[0:2]

smaller = cv2.pyrDown(image)
larger = cv2.pyrUp(image)

cv2.imshow("Original", image)

cv2.imshow("Smaller", smaller)
cv2.imshow("Larger", larger)

cv2.waitKey()
cv2.destroyAllWindows()
