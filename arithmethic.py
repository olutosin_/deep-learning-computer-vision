# Importing Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Read the image
image = cv2.imread("images/input.jpg")
height, width = image.shape[0:2]

# Create another image
image2 = np.ones(image.shape, dtype=np.uint8) * 75

# Operations
cv2.add(image, image2)
cv2.subtract(image, image2)

# Bitwise Operation
Or = cv2.bitwise_or(np.zeros((10, 10), dtype=np.uint8), np.ones((10, 10), dtype=np.uint8))
And = cv2.bitwise_or(np.zeros((10, 10), dtype=np.uint8), np.ones((10, 10), dtype=np.uint8))
Xor = cv2.bitwise_or(np.zeros((10, 10), dtype=np.uint8), np.ones((10, 10), dtype=np.uint8))
Not = cv2.bitwise_not(np.ones((10, 10), dtype=np.uint8))







