# Importing Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Read the image
image = cv2.imread("images/input.jpg")
height, width = image.shape[0:2]

Ty = height/4
Tx = width/4
"""
    Translational Matrix
    |1 0 Tx|
 T= |0 1 Ty|
 
"""

# Translational Matrix
T = np.float32([[1, 0, Tx], [0, 1, Ty]])

# Affine Transform
image_transform = cv2.warpAffine(image, T, (width, height))
cv2.imshow("Transformed Image", image_transform)

cv2.waitKey()
cv2.destroyAllWindows()
