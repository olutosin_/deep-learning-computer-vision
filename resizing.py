# Importing Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np
"""
cv2.INTER_AREA
cv2.INTER_LINEAR
cv2.INTER_CUBIC
cv2.INTER_LANCZOS4
"""

# Read the image
image = cv2.imread("images/input.jpg")
height, width = image.shape[0:2]

# Resizing
Resized_Image = cv2.resize(image, None, fx=2, fy=2, interpolation=cv2.INTER_LANCZOS4)
cv2.imshow("Resized Image", Resized_Image)

cv2.waitKey()
cv2.destroyAllWindows()



