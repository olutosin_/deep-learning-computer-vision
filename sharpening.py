# loading libraries

import cv2
import numpy as np

# making a square
obj = cv2.imread("./images/input.jpg")
cv2.imshow("Original", obj)

# kernel
kernel_shapening = np.array([[-1,-1,-1],
                            [-1,9,-1],
                            [-1,-1,-1]]
                            )

sharpened = cv2.filter2D(obj, -1, kernel_shapening)
cv2.imshow("sharpened", sharpened)
cv2.waitKey(0)
cv2.destroyAllWindows()


