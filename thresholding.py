# loading libraries

import cv2
import numpy as np

# making a square
obj = cv2.imread("./images/input.jpg", 0)
cv2.imshow("Original", obj)
cv2.waitKey()
cv2.destroyAllWindows()

# Thresholding is an act of converting an image into it binary form
# Adaptive thresholding or binarilization
# image, threshold value, ,max value and type

ret,thresh1 = cv2.threshold(obj, 127, 255, cv2.THRESH_BINARY)
cv2.imshow("Threshold Binary", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# Binary Inverse
ret,thresh1 = cv2.threshold(obj, 127, 255, cv2.THRESH_BINARY_INV)
cv2.imshow("Threshold Binary INV", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# Binary Truc
ret,thresh1 = cv2.threshold(obj, 127, 255, cv2.THRESH_TRUNC)
cv2.imshow("Threshold Binary TRUNC", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# Binary To Zero 
ret,thresh1 = cv2.threshold(obj, 127, 255, cv2.THRESH_TOZERO)
cv2.imshow("Threshold Binary TRUNC", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# Binary To Zero Inv
ret,thresh1 = cv2.threshold(obj, 127, 255, cv2.THRESH_TOZERO_INV)
cv2.imshow("Threshold Binary TRUNC", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# Adaptive thresholding
image = cv2.imread("./images/Origin_of_Species.jpg", 0)

# Always a good practice to blur images(removes noise)
image = cv2.GaussianBlur(image, (3,3), 0)

thresh1 = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 3, 5)
cv2.imshow("Threshold Binary TRUNC", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# Adaptive thresholding
image = cv2.imread("./images/Origin_of_Species.jpg", 0)

# Always a good practice to blur images(removes noise)
image = cv2.GaussianBlur(image, (3,3), 0)

thresh1 = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 3, 5)
cv2.imshow("Threshold", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()

# OTSU thresholding
image = cv2.imread("./images/Origin_of_Species.jpg", 0)

# Always a good practice to blur images(removes noise)
image = cv2.GaussianBlur(image, (3,3), 0)
thresh1 = cv2.threshold(image, 0,  255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow("Threshold", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()


# Otsu's thresholdibg after Gaussian blurring
# OTSU thresholding
image = cv2.imread("./images/Origin_of_Species.jpg", 0)

# Always a good practice to blur images(removes noise)
image = cv2.GaussianBlur(image, (3,3), 0)

_,thresh1 = cv2.threshold(image, 0,  255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow("Threshold Binary TRUNC", thresh1)
cv2.waitKey()
cv2.destroyAllWindows()


