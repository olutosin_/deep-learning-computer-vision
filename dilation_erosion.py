# Dilation adds more pixels at the boundary
# Erosion removes pixels from the boundary of an image
# Opening is Erosion followed by Dilation
# Closing Dilation followed by erosion

# Morphological Operations (Opening and Closing)

import cv2
import numpy as np

# making a square
obj = cv2.imread("./images/opencv_inv.png", 0)

cv2.imshow("Original", obj)
cv2.waitKey()
cv2.destroyAllWindows()

# kernel
kernel = np.ones((5,5), dtype=np.uint8)

# Erode
obj = cv2.imread("./images/opencv_inv.png", 0)
erotion = cv2.erode(obj, kernel, iterations = 1)
cv2.imshow("Erode", erotion)
cv2.waitKey()
cv2.destroyAllWindows()


# Erode
obj = cv2.imread("./images/opencv_inv.png", 0)
dilation = cv2.dilate(obj, kernel, iterations = 1)
cv2.imshow("Dilate", dilation)
cv2.waitKey()
cv2.destroyAllWindows()

# Good for removing noise
obj = cv2.imread("./images/opencv_inv.png", 0)
opening = cv2.morphologyEx(obj, cv2.MORPH_OPEN, kernel)
cv2.imshow("Opening", opening)
cv2.waitKey()
cv2.destroyAllWindows()

# Good for removing noise
obj = cv2.imread("./images/opencv_inv.png", 0)
closing = cv2.morphologyEx(obj, cv2.MORPH_CLOSE, kernel)
cv2.imshow("Closing", opening)
cv2.waitKey()
cv2.destroyAllWindows()


