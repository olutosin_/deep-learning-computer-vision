# Importing Libraries
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Read the image
image = cv2.imread("images/input.jpg")
height, width = image.shape[0:2]

start_row, start_col = int(height * 0.25), int(width * 0.25)
end_row, end_col = int(height * 0.75), int(width * 0.75)

# Cropping
cropped = image[start_row:end_row, start_col:end_col]

cv2.imshow("Original", image)
cv2.imshow("Cropped", cropped)

cv2.waitKey()
cv2.destroyAllWindows()
