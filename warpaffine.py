import cv2
import numpy as np
import matplotlib.pyplot as plt
import imutils

image = cv2.imread("./images/scan.jpg")

cv2.imshow("Original", image)
cv2.waitKey(0)
cv2.destroyAllWindows()

# ALgorithm that gets the coordinates

# Coordinate of the 4 corners of the image (clockwise)
point_A = np.float32([[320,15], [700,215], [86,610], [530,780]])

# coordinates of desired outputs
point_B = np.float32([[0,0], [420,0], [0,594], [420, 594]])

# calculating perspective transformation matrix
M = cv2.getPerspectiveTransform(point_A, point_B)

warp = cv2.warpPerspective(image, M, (420,594))
cv2.imshow("warped", warp)
cv2.waitKey(0)
cv2.destroyAllWindows()


# Warp Affine (needs 3 coodinates to get correct transform)



image = cv2.imread("./images/ex2.jpg")

# Coordinate of the 4 corners of the image (clockwise)
point_A = np.float32([[320,15], [700,215], [86,610]])

# coordinates of desired outputs
point_B = np.float32([[0,0], [420,0], [0,594]])

# calculating perspective transformation matrix
M = cv2.getAffineTransform(point_A, point_B)

warp = cv2.warpPerspective(image, M, (420,594))
cv2.imshow("warped", warp)
cv2.waitKey(0)
cv2.destroyAllWindows()