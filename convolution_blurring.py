# loading libraries

import cv2
import numpy as np

# making a square
image = cv2.imread("./images/elephant.jpg")
B, G, R = cv2.split(image)
cv2.imshow("B", B)
cv2.imshow("B", G)
cv2.imshow("B", R)

merged = cv2.merge([B, G, R])

cv2.waitKey(0)
cv2.destroyAllWindows()

# creating 3 x 3 kernel
kernel_3x3 = np.ones((3,3), dtype=np.float32) / 9

# blurring
blurred = cv2.filter2D(image, -1, kernel_3x3)
cv2.imshow("Blurred", blurred)
cv2.waitKey(0)
cv2.destroyAllWindows()

# creating 7 x 7 kernel
image2 = cv2.imread("./images/elephant.jpg")
kernel_7x7 = np.ones((7,7), dtype=np.float32) / 49
blurred2 = cv2.filter2D(image2, -1, kernel_7x7)
cv2.imshow("Blurred", blurred2)
cv2.waitKey(0)
cv2.destroyAllWindows()

# avaraging filter (image, box size)
image3 = cv2.imread("./images/elephant.jpg")
avg_output = cv2.blur(image3, (3,3))
cv2.imshow("Blurred", avg_output)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Gaussian filter
image4 = cv2.imread("./images/elephant.jpg")
avg_output = cv2.GaussianBlur(image4, (3,3), 0)
cv2.imshow("Blurred", avg_output)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Median blur
image5 = cv2.imread("./images/elephant.jpg")
avg_output = cv2.medianBlur(image5, 5)
cv2.imshow("Blurred", avg_output)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Bilateral blur
image5 = cv2.imread("./images/elephant.jpg")
avg_output = cv2.bilateralFilter(image5, 9, 75, 75)
cv2.imshow("Blurred", avg_output)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Image Denoising: Image, None,
image6 = cv2.imread("./images/elephant.jpg")
denoised = cv2.fastNlMeansDenoisingColored(image6, None, 6, 6, 7, 21)
cv2.imshow("Denoised", denoised)
cv2.waitKey(0)
cv2.destroyAllWindows()


