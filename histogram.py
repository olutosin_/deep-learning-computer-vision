# Import Libraries
import cv2
from matplotlib import pyplot as plt

# Load the image
image = cv2.imread("images/input.jpg")
# image, channels, mask,  size, histSize, range
histogram = cv2.calcHist([image], [0], None, [256], [0, 256])

# Plotting histogram
plt.hist(image.ravel(), 256, [0, 256])
plt.show()
color = ('b', 'g', 'r')

# Viewing individual color
for i,col in enumerate(color):
    histogram2 = cv2.calcHist([image], [i], None, [256], [0,256])
    plt.plot(histogram2, color=col)
    plt.show()
