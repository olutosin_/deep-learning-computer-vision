# Edge is the boundary of an image
# Change in the image gradient

# Sobel
# Laplacian
# Canny: most accurate 
"""
1. Applies Gaussian blur
2. Finds intensity gradient of the images
3. Aplies maximum suppression to remove non edge
4. Applies thresholding to detect the edge
"""

import cv2
import numpy as np

image = cv2.imread("./images/input.jpg", 0)
height, width = image.shape

# extract sobel edges
sobel_x = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=5)
sobel_y = cv2.Sobel(image, cv2.CV_64F, 1,0 , ksize=5)

cv2.imshow("Original", image)
cv2.waitKey(0)

cv2.imshow("Sobel X", sobel_x)
cv2.waitKey(0)

cv2.imshow("Sobel Y", sobel_y)
cv2.waitKey(0)

sobel_OR = cv2.bitwise_or(sobel_x, sobel_y)
cv2.imshow("Sobel OR", sobel_OR)
cv2.waitKey(0)

# Laplacian
image = cv2.imread("./images/input.jpg", 0)
laplacian = cv2.Laplacian(image, cv2.CV_64F)
cv2.imshow("Laplacian", laplacian)
cv2.waitKey(0)

# canny
image = cv2.imread("./images/input.jpg", 0)
laplacian = cv2.Canny(image, 20, 70)
cv2.imshow("Canny", canny)
cv2.waitKey(0)

cv2.destroyAllWindows()





