# Live sketch
import cv2
import numpy as np

def sketch(image):
    # converts to gray scale
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # applying gaussian blur to clean image up
    img_gray_blur = cv2.GaussianBlur(img_gray, (5,5), 0)
       
    # Extract Edges
    canny_edges = cv2.Canny(img_gray_blur, 10, 10)
    
    # do inverse binarize
    ret, mask = cv2.threshold(canny_edges, 70, 255, cv2.THRESH_BINARY_INV)
    
    return mask
    

# Init the webcam
cap = cv2.VideoCapture(0)

while(cap.isOpened()):
    ret, frame = cap.read()
    print(frame.shape)
    cv2.imshow("Live Sketch", sketch(frame))
    
    if cv2.waitKey(1) == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()


    

