# Project WordCloud using python
# Name Young Olutosin
# Date 11-22-2019 


# import libraries
import urllib.request
import nltk
from bs4 import BeautifulSoup as bs
import re
import matplotlib.pyplot as plt
from wordcloud import WordCloud


# container to hold content
main_reviews = []
url = "https://sellercentral.amazon.com/forums/t/universal-link-to-leave-a-product-review/143845"

# loop through site to get content page 1 to 20
for i in range(1,20):
	ip = []
	response = urlilib.request.urlopen(url)
	soup = bs(response.content, "html.parser")
	reviews = soup.findAll("span", attrs={"class", "a-size-base review-text" })
	for i in range(len(reviews)):
		ip.append(reviews[i].text)
	main_reviews = main_reviews + ip

# writing reviews to a file
with open("review.txt", "w", encoding= "utf8") as output:
	output.write(str(main_reviews))

	
# converting to string
review_string = " ".join(main_reviews)

# processing with  re
review_string = re.sub("[^a-zA-Z " "] + ", ", ", review_string).lower() 
# removes number(note ^ is not added 
#to the beginning of 0-9)  
review_string = re.sub("[0-9 " "] + "," ", review_string)  

# splits into words separated by space
review_word = review_string.split(" ")

# download stopwords
# nltk.downlaod(stopwords)
# from nltk import stopwords
# stop_words = stopwords.words('english')
# or load the stopwords from local pc
with open("path to stopwords.txt downloaded", "r") as sw:
	stopwords = sw.read()

stopwords = stopwords.split("\n")
review_words = [w for w in review_words if not w in stopwords]
review_words = " ".join(review_words)

# plotting word cloud
word_cloud = WordCloud(background_cloud = 'dark',
	                   width = 1800,
	                   height = 1400,
	                   ).generate(review_words)
plt.imshow(word_cloud)


# positive words
with open("path to positive.txt downloaded", "r") as ps:
	positive_word = ps.read().split("\n")

positive_word = positive_word[36:]

# negative words
with open("path to positive.txt downloaded", "r") as ng:
	negative_word = ps.read().split("\n")

negative_word = positive_word[37:]

neg_word = " ".join([word for word in review_words if word in negative_word])
pos_word = " ".join([word for word in review_words if word in positive_word])

# plotting word cloud for positive words
word_cloud = WordCloud(background_cloud = 'dark',
	                   width = 1800,
	                   height = 1400,
	                   ).generate(neg_word)
plt.figure(2)
plt.imshow(neg_cloud)

# plotting word cloud for negativce words
word_cloud = WordCloud(background_cloud = 'dark',
	                   width = 1800,
	                   height = 1400,
	                   ).generate(pos_word)

plt.figure(3)
plt.imshow(pos_word)

plt.figure(4)

plt.figure(5)
plt.plot(neg_word)

# help on how to use
help(WordCloud)









